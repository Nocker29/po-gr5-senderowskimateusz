package pl.edu.uwm.wmii.kotewa.laboratorium00;
import java.util.*;
import java.lang.Math.*;
public class Main {
    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random a=new Random();
        for(int i=0;i<n;i++){
            tab[i] = a.nextInt(maxWartosc -minWartosc+1)-Math.abs(minWartosc);
        }
    }
    public static int ileNieparzystych (int tab[])
    {
        int len=tab.length;
        int ile=0;
        for(int i=0;i<len;i++){
            if(tab[i]%2==1 || tab[i]%2==-1){
                ile++;
            }
        }
        return ile;
    }
    public static int ileParzystych (int tab[])
    {
        int len=tab.length;
        int ile=0;
        for(int i=0;i<len;i++){
            if(tab[i]%2==0){
                ile++;
            }
        }
        return ile;
    }
    public static int ileDodatnich (int tab[])
    {
        int len=tab.length;
        int ile=0;
        for(int i=0;i<len;i++)
        {
            if(tab[i]>0) {
                ile++;
            }
        }
        return ile;
    }
    public static int ileUjemnych (int tab[])
    {
        int len=tab.length;
        int ile=0;
        for(int i=0;i<len;i++)
        {
            if(tab[i]<0) {
                ile++;
            }
        }
        return ile;
    }
    public static int ileZerowych (int tab[])
    {
        int len=tab.length;
        int ile=0;
        for(int i=0;i<len;i++)
        {
            if(tab[i]==0) {
                ile++;
            }
        }
        return ile;
    }
    public static int ileMaksymalnych (int tab[])
    {
        int len=tab.length;
        int najwieksza=-999,ile_naj=0;
        for(int i=0;i<len;i++){
            if(najwieksza<tab[i]){
                najwieksza=tab[i];
                ile_naj=1;
            }
            else if(najwieksza==tab[i]){
                ile_naj++;
            }
        }
        return ile_naj;
    }
    public static int sumaDodatnich (int tab[])
    {
        int len=tab.length;
        int suma=0;
        for(int i=0;i<len;i++)
        {
            if(tab[i]>0)
            {
                suma+=tab[i];
            }
        }
        return suma;
    }
    public static int sumaUjemnych (int tab[])
    {
        int len=tab.length;
        int suma=0;
        for(int i=0;i<len;i++)
        {
            if(tab[i]<0)
            {
                suma+=tab[i];
            }
        }
        return suma;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich (int tab[])
    {
        int len=tab.length;
        int dlugosc=0,wynik=0;
        for(int i=0;i<len;i++)
        {
            if(tab[i]>0)
            {
                dlugosc++;
            }
            else
            {
                if(wynik<dlugosc)
                {
                    wynik=dlugosc;
                }
                dlugosc=0;
            }
        }
        return wynik;
    }
    public static void main(String[] args) {
        int n;
        System.out.println("Podaj n: ");
        Scanner sc=new Scanner(System.in);
        n=sc.nextInt();
        int[] liczby = new int[n];
        generuj(liczby,n,-999,999);
        for(int i=0;i<n;i++) {
            System.out.println(i+1+": "+liczby[i]);
        }
        System.out.println("Parzyste: "+ileParzystych(liczby));
        System.out.println("Nieparzyste: "+ileNieparzystych(liczby));
        System.out.println("Dodatnie: "+ileDodatnich(liczby));
        System.out.println("Ujemne: "+ileUjemnych(liczby));
        System.out.println("Zera: "+ileZerowych(liczby));
        System.out.println("Maksymalna występuje: "+ileMaksymalnych(liczby)+" razy");
        System.out.println("Suma dodatnich liczb: "+sumaDodatnich(liczby));
        System.out.println("Suma ujemnych liczb: "+sumaUjemnych(liczby));
        System.out.println("Długość maksymalnego ciągu dodatnich liczb: "+dlugoscMaksymalnegoCiaguDodatnich(liczby));
    }
}
